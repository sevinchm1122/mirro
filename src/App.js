import React from "react";
import {FaFacebookSquare} from "react-icons/fa";
import {FaInstagram} from "react-icons/fa";
import {FaTwitter} from "react-icons/fa";
import {FaCartArrowDown} from "react-icons/fa";
import rasm from './assets/image/mirro-Logotype-Green.png'
import person from './assets/image/iMAGE.png'
import line from './assets/image/Fill-1.png'
import f1 from './assets/image/frame.png'
import f2 from './assets/image/frame2.png'
import f3 from './assets/image/frame3.png'
import f4 from './assets/image/frame4.png'
import f5 from './assets/image/frame5.png'
import f6 from './assets/image/frame6.png'
import f7 from './assets/image/frame7.png'
import strelka from './assets/image/strelka.png'
import card1 from './assets/image/card1.png'
import card2 from './assets/image/card2.png'
import card3 from './assets/image/card3.png'
import logo from './assets/image/logo.png'
import './app.css'

function App() {
  return (
   <div>
       <div className='container mx-auto'>
           <ul className='flex justify-around m-6 text-lime-700 gap-3 '>
               <img src={rasm} alt="" className=''/>

               <li><a href="">Presets</a></li>
               <li ><a href="">Prints</a></li>
               <li><a href="">Store</a></li>
               <li ><a href="">About</a></li>
               <li ><a href="">Contact</a></li>
               <button className='bg-green-900 text-white px-3 rounded-md'><FaCartArrowDown className='text-black inline mr-2'></FaCartArrowDown>CART</button>
           </ul>
       </div>
       <div className='page1 sm:tablet'>
       <div className="bg-[url('./assets/image/rasm.png')] bg-no-repeat bg-right-top ">
           <div className='container mx-auto '>
           <div className='grid gap-40 grid-cols-2 items-center'>
               <div className=''>
                   <h1 className='font-serif  mb-5 text-lime-700'>Hi, I'm James Mirro & <br/> I'm a photographer.</h1>
                   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere.</p>
             <button className='p-2 mt-5 bg-green-900 rounded-md text-white'>Read more</button>
               </div>
               <div>
                   <img src={person} alt="" className='mt-24 mb-16 ' />
               </div>
           </div>
           </div>
       </div>
       </div>
       <div className='page3 sm:tablet object-center'>
           <div className='container mx-auto pb-32 '>
               <img src={line} alt="" className='line'/>
               <h1 className='text-center text-lime-700  mb-4 mt-4 text-2xl font-serif md:text-sm '>Services</h1>
               <p className='text-center  ' >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim <br/>
               in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor <br/> interdum nulla.</p>

               <div className='flex flex-wrap gap-4  mt-20 ml-14'>
                   <div className='bg-stone-100 w-96 p-8 rounded-xl '>
                       <img src={f1} alt="" className='pt-4'/>
                       <h1 className='text-lime-700 font-serif text-xl mb-5'>Wedding Photography</h1>
                       <p className='text-tiny mb-5'>Wedding photography is often called the bread and butter of photographers — but that doesn't mean it has to be boring. Quite the opposite!</p>
                       <a href="#" className='text-lime-700'>Read more<img src={strelka} alt="" className='inline pl-3'/></a>
                   </div>
                   <div className='bg-stone-100 w-96 p-8 rounded-xl '>
                       <img src={f2} alt="" className='pt-4'/>
                       <h1 className='text-lime-700 font-serif text-xl mb-5'>Sports Photography</h1>
                       <p className='text-tiny mb-5'>Sports photography is awesome. Sitting on the sidelines with a massive telephoto allows me to get right into the action — I love shooting sports where there's a lot of explosive movement.</p>
                       <a href="#" className='text-lime-700'>Read more<img src={strelka} alt="" className='inline pl-3'/></a>
                   </div>
                   <div className='bg-stone-100 w-96 p-8 rounded-xl '>
                       <img src={f3} alt="" className='pt-4'/>
                       <h1 className='text-lime-700 font-serif text-xl mb-5'>Portrait Photography</h1>
                       <p className='text-tiny mb-5'>Who doesn't love portrait photography? I love being able to capture that expression, quirk, smile or whatever, that sort of defines a person.</p>
                       <a href="#" className='text-lime-700'>Read more<img src={strelka} alt="" className='inline pl-3'/></a>
                   </div>
               </div>
               <div className='flex flex-wrap gap-4  mt-5 ml-14'>
                   <div className='bg-stone-100 w-96 p-8 rounded-xl'>
                       <img src={f4} alt="" className='pt-4'/>
                       <h1 className='text-lime-700 font-serif text-xl mb-5'>Architecture Photography</h1>
                       <p className='text-tiny mb-5'>The nice thing about shooting architecture (in my opinion) is that it's so easy to do. Seriously, just pick up your camera and get out there.</p>
                       <a href="#" className='text-lime-700'>Read more<img src={strelka} alt="" className='inline pl-3'/></a>
                   </div>
                   <div className='bg-stone-100 w-96 p-8 rounded-xl'>
                       <img src={f5} alt="" className='pt-4'/>
                       <h1 className='text-lime-700 font-serif text-xl mb-5'>Animal Photography</h1>
                       <p className='text-tiny mb-5'>Whenever I'm travelling I always make sure to pack gear that allows me to capture the local wildlife. The eagle that takes flight in this picture is probably my most prized image.</p>
                       <a href="#" className='text-lime-700'>Read more<img src={strelka} alt="" className='inline pl-3'/></a>
                   </div>
                   <div className='bg-stone-100 w-96 p-8 rounded-xl'>
                       <img src={f6} alt="" className='pt-4' />
                       <h1 className='text-lime-700 font-serif text-xl mb-5'>Food Photography</h1>
                       <p className='text-tiny mb-5'>I do food photography for blogs, restaurants, influencers, YouTube channels — but most importantly (if I manage to cook something that looks good) — I do food photography for myself.</p>
                       <a href="#" className='text-lime-700'>Read more<img src={strelka} alt="" className='inline pl-3'/></a>
                   </div>
               </div>

                   <div className='bg-stone-100 w-96 p-8 rounded-xl mt-5 ml-14'>
                       <img src={f7} alt="" className='pt-4'/>
                       <h1 className='text-lime-700 font-serif text-xl mb-5'>Nature Photography</h1>
                       <p className='text-tiny mb-5'>Landscape photography just never gets old. Someone told me that those are the kinds of pictures you never look. Well, I don't agree.</p>
                       <a href="#" className='text-lime-700 '> Read more <img src={strelka} alt="" className='inline pl-2'/></a>
                   </div>


           </div>

       </div>
       <div className='page4 '>
       <div >
           <div className='container mx-auto p-20'>
               <img src={line} alt="" className='line'/>
               <h1 className='text-center text-red-600  mb-4 mt-4 text-2xl font-serif'>Preset Store</h1>
               <p className='text-center  ' >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim <br/>
                   in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor <br/> interdum nulla.</p>
               <div className='flex gap-4 mt-16 mb-20 '>
                   <div className=' w-1/2  bg-white rounded-b-lg'>
                       <img src={card1} alt="" className='w-full'/>
                       <h1 className='p-3 text-lime-700 font-serif text-xl'>Mirro Preset Pack Vol. 01</h1>
                       <p className='p-3 text-lime-800 pb-3'>The first volume is my go-to presets when shooting action sports. Just the right amount of contrast and highlights to make your photo (literally) pop!</p>
                   </div>   <div className=' w-1/2 bg-white rounded-b-lg '>
                       <img src={card2} alt="" className='w-full'/>
                       <h1 className='p-3 text-lime-700 font-serif text-xl'>Mirro Preset Pack Vol. 02</h1>
                       <p className='p-3 text-lime-800 pb-3'>Vol. 02 is dedicated solely to landscape photography. Pay attention to your exposure while out there and these presets will make your photos look great.</p>
                   </div>   <div className='w-1/2  bg-white rounded-b-lg'>
                       <img src={card3} alt="" className='w-full'/>
                       <h1 className='p-3 text-lime-700 font-serif text-xl'>Mirro Preset Pack Vol. 03</h1>
                       <p className='p-3 text-lime-800 pb-3'>These are a collection of my most used wedding presets — use with caution as you'll end up loving wedding photography.</p>
                   </div>
               </div>
               <a href="#"  className='text-lime-700  '>Preset store <img src={strelka} alt="" className='inline pl-2'/></a>
           </div>
       </div>
       </div>
       <div className='page5 pb-20'>
           <div className='container mx-auto '>
               <div className='bg-stone-100 w-3/5 rounded-lg mb-20 object-center'>
           <h1 className=' p-20 mt-32 text-2xl '>Wan't to work together? <br/> Follow the link and let's<button className='inline float-right  bg-green-900 text-white rounded-md p-2' >Contact</button> <br/> get in touch.
           </h1>
               </div>
           </div>

       </div>
       <div className='footer  bg-slate-900 pt-20 pb-20'>
           <div className='container mx-auto'>
               <img src={logo} alt="" className='object-center mb-20'/>
               <div className='flex flex-wrap justify-between '>
                   <ul >
                       <li className='text-orange-800 text-lg'>PAGES</li>
                       <li className='text-white'><a href="#">Home</a></li>
                       <li className='text-white'><a href="#">Store</a></li>
                       <li className='text-white'><a href="#">About</a></li>
                       <li className='text-white'><a href="#">Contact</a></li>
                   </ul>
                   <ul >
                       <li className='text-orange-800 text-lg'>Preset packs</li>
                       <li className='text-white'><a href="#">Mirro Preset Pack Vol. 01</a></li>
                       <li className='text-white'><a href="#">Mirro Preset Pack Vol. 02</a></li>
                       <li className='text-white'><a href="#">Mirro Preset Pack Vol. 03</a></li>
                       <li className='text-white'><a href="#">Mirro Preset Pack Vol. 04</a></li>
                   </ul>
                   <ul >
                       <li className='text-orange-800 text-lg'>Utility Pages</li>
                       <li className='text-white'><a href="#"></a>Instructions</li>
                       <li className='text-white'><a href="#"></a>Style guide</li>
                       <li className='text-white'><a href="#"></a>Licenses</li>
                       <li className='text-white'><a href="#"></a>Changelog</li>
                       <li className='text-white'><a href="#"></a>404</li>
                   </ul>
                   <ul>
                       <li className='text-orange-800 text-lg'>Newsletter</li>
                       <li className='text-white'><a href="#">Subscribe to my newsletter where I share news about upcoming printings and preset sales.</a></li>
                       <form>
                           <input type="email" placeholder='Email adress ' className='w-96 h-10 rounded-lg'/>

                       </form>
                   </ul>


               </div>
                   <div className='flex justify-start mt-20 pb-12 gap-3'>
                       <ul >
                           <li className='text-orange-800 text-lg mt-10'>Store categories</li>
                           <li className='text-white'><a href="#">Presents</a></li>
                           <li className='text-white'><a href="#">Prints</a></li>
                       </ul>
                       <ul>
                           <li className='text-orange-800 text-lg'>Business Areas</li>
                           <li className='text-white'><a href="#">Wedding Photography</a></li>
                           <li className='text-white'><a href="#">Sports Photography</a></li>
                           <li className='text-white'><a href="#">Portrait Photography</a></li>
                           <li className='text-white'><a href="#">Architecture Photography</a></li>
                           <li className='text-white'><a href="#">Animal Photography</a></li>
                           <li className='text-white'><a href="#">Food Photography</a></li>
                           <li className='text-white'><a href="#">Nature Photography</a></li>

                       </ul>
                   </div>

               <a href="#" className='text-white' >© Mirro Photography, LLC. All rights reserved. Powered by <a href="#">Webflow</a></a>
               <div className='flex justify-end '>
<FaFacebookSquare className='text-white inline mr-2'>
</FaFacebookSquare>
               <FaInstagram className='text-white inline mr-2'></FaInstagram>
               <FaTwitter className='text-white inline'></FaTwitter>
               </div>
           </div>
       </div>
   </div>
  );
}

export default App;
